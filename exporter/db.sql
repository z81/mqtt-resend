CREATE TABLE mqttdata (
  topic String,
  ts UInt64,
  v Float32,
  d Date MATERIALIZED toDate(ts),
  dt DateTime MATERIALIZED toDateTime(ts)
) ENGINE = MergeTree(d, (topic, dt), 8192);

-- CREATE TABLE data2 (
--   name UInt32,
--   user UInt32,
--   ts UInt64,
--   v1 Float32,
--   v2 Float32,
--   d Date MATERIALIZED toDate(ts),
--   dt DateTime MATERIALIZED toDateTime(ts)
-- ) ENGINE = MergeTree(d, (name, user, dt), 8192);