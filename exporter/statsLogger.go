package main

import (
	"time"
	"math"
	"fmt"
	"github.com/dustin/go-humanize"
)


type Stats struct {
	success   int
	recived   int
	startTime int64
	time      int64
}


func writeStats(stats *Stats) {
	timeDiff := float64(time.Now().Unix() - stats.startTime)
	recAvgMsgSec := int(math.Floor(float64(stats.recived) / timeDiff))
	sucAvgMsgSec := int(math.Floor(float64(stats.success) / timeDiff))

	fmt.Printf("Suc %[1]s, Rec %[2]s, Avg rec %[3]s/sec, avg suc %[4]s/sec \n",
		humanize.Comma(int64(stats.success)),
		humanize.Comma(int64(stats.recived)),
		humanize.Comma(int64(recAvgMsgSec)),
		humanize.Comma(int64(sucAvgMsgSec)),
	)
}

func initStatsTimer() *Stats {
	stats := Stats{
		success:   0,
		recived:   0,
		startTime: time.Now().Unix(),
		time:      time.Now().Unix(),
	}


	timer := time.NewTicker(statsLogInterval)

	go func() {
		for range timer.C {
			writeStats(&stats)
		}
	}()

	return &stats
}