package main

import (
	"database/sql"
	"fmt"
	"github.com/kshvakov/clickhouse"
)

type Connection clickhouse.Clickhouse

func connectClickhouse(dbUrl string) *sql.DB {
	connect, err := sql.Open("clickhouse", dbUrl)
	if err != nil {
		fmt.Println(err)
	}

	// Проверяем что подключение успешно
	if err := connect.Ping(); err != nil {
		if exception, ok := err.(*clickhouse.Exception); ok {
			fmt.Printf("[%d] %s \n%s\n", exception.Code, exception.Message, exception.StackTrace)
		} else {
			fmt.Println(err)
		}
		return nil
	}


	rows, err := connect.Query("CREATE TABLE mqttdata (topic String,ts UInt64, v Float32, d Date MATERIALIZED toDate(ts), dt DateTime MATERIALIZED toDateTime(ts) ) ENGINE = MergeTree(d, (topic, dt), 8192);")

	if err != nil {
	}
	fmt.Println(rows)

	return connect
}