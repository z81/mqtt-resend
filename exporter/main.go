package main

import (
	"database/sql"
	"log"
	"sync"
	"time"
	"strconv"
	"github.com/caarlos0/env"
	MQTT "github.com/eclipse/paho.mqtt.golang"
	"fmt"
)

var statsLogInterval =  60 * time.Second
var clientTopics = []string{
	"Prefix2/ds18b20/4",
	"Prefix2/ds18b20/8",
	"prefix1/dhtTemp/1",
	"Prefix2/ds18b20/2",
	"prefix1/dhtHumidity/1",
	"Prefix2/ds18b20/7",
	"prefix1/ds18b20/1",
	"prefix1/ds18b20/2",
	"Prefix2/ds18b20/3",
	"Prefix2/ds18b20/1",
	"Prefix2/ds18b20/6",
	"Prefix2/ds18b20/5",
	"10-kotelnya/ds18b20/4",
	"10-kotelnya/ds18b20/8",
	"10-8/dhtTemp/1",
	"10-kotelnya/ds18b20/2",
	"10-8/dhtHumidity/1",
	"10-kotelnya/ds18b20/7",
	"10-8/ds18b20/1",
	"10-8/ds18b20/2",
	"10-kotelnya/ds18b20/3",
	"10-kotelnya/ds18b20/1",
	"10-kotelnya/ds18b20/6",
	"10-kotelnya/ds18b20/5",
	"test/co2/1",
}

type config struct {
	DBUrl        string        `env:"DB_URL" envDefault:"tcp://clickhouse:9000?debug=false"`
	MQTTUrl      string        `env:"MQTT_URL" envDefault:"tcp://fractal.tools:1883"`
}


func write(connect *sql.DB, stats *Stats, topic string, value float64) {
	var (
		tx, _   = connect.Begin()
		stmt, _ = tx.Prepare("INSERT INTO mqttdata (topic, ts, v) VALUES (?, ?, ?)")
	)

	if _, err := stmt.Exec(
		topic,
		stats.time,
		value,
	); err != nil {
		log.Fatal(err)
	}

	stats.time = time.Now().Unix()

	if err := tx.Commit(); err != nil {
		log.Fatal(err)
	}

	stats.success += 1
}




func main() {
	cfg := config{}
	err := env.Parse(&cfg)
	if err != nil {
		fmt.Printf("%+v\n", err)
	}


	stats := initStatsTimer()

	connect := connectClickhouse(cfg.DBUrl)

	mqttConnect(cfg.MQTTUrl, func(client MQTT.Client, msg MQTT.Message) {
		stats.recived += 1

		var topic = string(msg.Topic())
		var value, err = strconv.ParseFloat(string(msg.Payload()), 64)

		if err == nil && connect != nil {
			write(connect, stats, topic, value)
		}

	})



	var wg sync.WaitGroup
	wg.Add(1)
	wg.Wait()
}

