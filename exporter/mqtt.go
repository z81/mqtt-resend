package main

import (
	"time"
	"log"
	"fmt"
	"os"
	MQTT "github.com/eclipse/paho.mqtt.golang"
)


func mqttConnect(mqttServer string, onPublish MQTT.MessageHandler) {
	opts := MQTT.NewClientOptions().AddBroker(mqttServer)
	opts.SetAutoReconnect(true)
	opts.SetKeepAlive(time.Second * 60)
	opts.SetCleanSession(true)

	opts.SetOnConnectHandler(func (c MQTT.Client) {
		log.Printf("MQTT Connected")

		for _, t := range clientTopics {
			if token := c.Subscribe(t, 0, nil); token.Wait() && token.Error() != nil {
				fmt.Println(token.Error())
				os.Exit(1)
			}
		}

		return
	})

	opts.SetConnectionLostHandler(func (c MQTT.Client, err error) {
		fmt.Println(err.Error())
		fmt.Printf("E! MQTT Connection lost\nerror: %s\nMQTT Client will try to reconnect")
		return
	})

	opts.SetClientID(fmt.Sprintf("mqtt_saver_%d%d", os.Getpid(), time.Now().Unix()))

	opts.SetDefaultPublishHandler(onPublish)

	c := MQTT.NewClient(opts)
	if token := c.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
}