package main

import (
	"encoding/json"
	"database/sql"
	"fmt"
	"log"
	"sync"
	"github.com/kshvakov/clickhouse"
	"net/http"
	"strconv"
)

func q(connect *sql.DB, datetime int64, dateRange string) string {
	limit := 5000

	var avgFunction = "toStartOfFifteenMinutes(dt)"
	var startDate = datetime
	var endDate = startDate


	if (dateRange == "30min") {
		endDate += 1800
		avgFunction = "round(ts / 30, 0) * 30"
	}
	if (dateRange == "1hour") {
		endDate += 3600
		avgFunction = "round(ts / 60, 0) * 60"
	}
	if (dateRange == "12hours") {
		avgFunction = "round(ts / 600, 0) * 600"
		endDate += 43200
	}
	if (dateRange == "day") {
		endDate += 86400
		avgFunction = "toStartOfFifteenMinutes(dt)"
	}
	if (dateRange == "week") {
		endDate += 604800
		avgFunction = "round(ts / 6000, 0) * 6000"
	}
	if (dateRange == "month") {
		endDate += 2592000
		avgFunction = "round(ts / 30000, 0) * 30000"
	}


	s := fmt.Sprintf("SELECT %s as t, round(avg(v), 2) as vv, topic FROM mqttdata WHERE ts >= %d and ts <= %d GROUP BY topic, t ORDER BY topic, t LIMIT %d", avgFunction, startDate, endDate, limit)
	fmt.Println(s)
	rows, err := connect.Query(s)

	if err != nil {
		log.Fatal(err)
	}

	columns, _ := rows.Columns()
	count := len(columns)

	var v struct {
		Data []interface{} // `json:"data"`
	}

	for rows.Next() {
		values := make([]interface{}, count)
		valuePtrs := make([]interface{}, count)
		for i, _ := range columns {
			valuePtrs[i] = &values[i]
		}
		if err := rows.Scan(valuePtrs...); err != nil {
			log.Fatal(err)
		}
	
		//Created a map to handle the issue
		var m map[string]interface{}
		m = make(map[string]interface{})
		for i := range columns {
			m[columns[i]] = values[i]
		}
		v.Data = append(v.Data, m)
	}

	jsonMsg, _ := json.Marshal(v)
	
	return string(jsonMsg)
}



func main() {
	// Подключение
	connect, err := sql.Open("clickhouse", "tcp://clickhouse:9000?debug=false")
	if err != nil {
		log.Fatal(err)
	}

	// Проверяем что подключение успешно
	if err := connect.Ping(); err != nil {
		if exception, ok := err.(*clickhouse.Exception); ok {
			fmt.Printf("[%d] %s \n%s\n", exception.Code, exception.Message, exception.StackTrace)
		} else {
			fmt.Println(err)
		}
		return
	}


	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")

		datetime := r.URL.Query().Get("datetime")
		rangeDate := r.URL.Query().Get("range")
	
		if (datetime == "" || rangeDate == "") {
			w.Write([]byte("{}"))
			return
		}

		dateInt, err := strconv.ParseInt(datetime, 10, 64)
	
		if (err != nil || dateInt == 0) {
			w.Write([]byte("{}"))
			return
		}

		w.Write([]byte(q(connect, dateInt / 1000, rangeDate)))
	})
	log.Fatal(http.ListenAndServe(":5394", nil))

	var wg sync.WaitGroup
	wg.Add(1)
	wg.Wait() 
}